# buy-mask

### インストール

```
git clone https://k_maruta@bitbucket.org/k_maruta/buy-mask.git
cd buy-mask
npm i
```

### 起動

```
npm run forever-start
```

### 終了

```
npm run forever-stop
```

### 起動状況確認

```
npm run forever-show
```

### ログ

```
npm run forever-log
```

## 開発

### ビルド

```
npm run build
```

