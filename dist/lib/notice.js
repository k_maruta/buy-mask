"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const https_1 = __importDefault(require("https"));
const utils = __importStar(require("./utils"));
exports.colors = {
    good: '#2FA44F',
    worning: '#DE9E31',
    danger: '#D50200',
};
function slack(params) {
    return __awaiter(this, void 0, void 0, function* () {
        utils.log('slack notice');
        const host = 'hooks.slack.com';
        const path = '/services/TCF3FR9T9/B011BBCAS8M/dBed3Sa4zTjGhHR0JcsEdtXP';
        let data = JSON.stringify({
            text: `${utils.nowStr()} - ${params.title}`,
            attachments: [
                {
                    title: params.subtitle,
                    text: params.message,
                    color: exports.colors[params.color]
                }
            ]
        });
        let options = {
            hostname: host,
            port: 443,
            path: path,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(data)
            }
        };
        return new Promise((resolve) => {
            let req = https_1.default.request(options, (res) => {
                console.log('status code : ' + res.statusCode);
                res.setEncoding('utf8');
                let body = '';
                res.on('data', (chunk) => {
                    body += chunk;
                });
                res.on('end', () => {
                    console.log(body);
                    resolve();
                });
            });
            req.on('error', (e) => {
                console.error(e);
                resolve();
                ;
            });
            req.write(data);
            req.end();
        });
    });
}
exports.slack = slack;
//# sourceMappingURL=notice.js.map