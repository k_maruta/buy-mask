"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function dateFormat(date, format) {
    format = format.replace(/yyyy/g, date.getFullYear().toString());
    format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
    format = format.replace(/dd/g, ('0' + date.getDate()).slice(-2));
    format = format.replace(/HH/g, ('0' + date.getHours()).slice(-2));
    format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
    format = format.replace(/ss/g, ('0' + date.getSeconds()).slice(-2));
    format = format.replace(/SSS/g, ('00' + date.getMilliseconds()).slice(-3));
    return format;
}
exports.dateFormat = dateFormat;
;
function nowStr() {
    return dateFormat(new Date(), 'yyyy/MM/dd HH:mm:ss');
}
exports.nowStr = nowStr;
function log(msg) {
    console.log(`${nowStr()}: ${msg}`);
}
exports.log = log;
//# sourceMappingURL=utils.js.map