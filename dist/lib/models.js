"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const utils = __importStar(require("./utils"));
const infoPath = path_1.default.join(__dirname, '../../data/info.json');
class InfoModel {
    constructor() {
        this.prevCount = null;
        //
    }
    static read() {
        const info = new InfoModel();
        try {
            const json = fs_1.default.readFileSync(infoPath, 'utf-8');
            const data = JSON.parse(json);
            info.prevCount = data.prevCount;
        }
        catch (e) {
            //
        }
        return info;
    }
    save() {
        try {
            fs_1.default.writeFileSync(infoPath, JSON.stringify(this, null, 4));
            utils.log('saved info.json');
        }
        catch (e) {
            console.error(e);
        }
    }
}
exports.InfoModel = InfoModel;
//# sourceMappingURL=models.js.map