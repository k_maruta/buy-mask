"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const phantomjs = require('phantomjs-prebuilt');
const webdriverio = __importStar(require("webdriverio"));
const utils = __importStar(require("./utils"));
class Costco {
    constructor() {
        this.baseUrl = 'https://www.costco.co.jp';
        this.userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.1.54 (KHTML, like Gecko) Version/9.0 Safari/601.1.54";
        this.phantom = null;
        this.browser = null;
        //
    }
    static create() {
        return __awaiter(this, void 0, void 0, function* () {
            const instance = new Costco();
            yield instance.setup();
            return instance;
        });
    }
    setup() {
        return __awaiter(this, void 0, void 0, function* () {
            const wdOpts = {
                hostname: 'localhost',
                port: 4444,
                capabilities: {
                    browserName: 'phantomjs',
                    "phantomjs.page.settings.userAgent": this.userAgent,
                },
                logLevel: 'silent',
            };
            return new Promise((resolve, reject) => {
                try {
                    phantomjs.run('--webdriver=4444')
                        .then((phantom) => __awaiter(this, void 0, void 0, function* () {
                        const browser = yield webdriverio.remote(wdOpts);
                        this.browser = browser;
                        this.phantom = phantom;
                        utils.log('initialized browser');
                        resolve();
                    }))
                        .catch((e) => {
                        reject(e);
                    });
                }
                catch (e) {
                    reject(e);
                }
            });
        });
    }
    search(key) {
        return __awaiter(this, void 0, void 0, function* () {
            utils.log('searching...');
            let searchCount = null;
            try {
                const url = `${this.baseUrl}/search/?text=${key.split(' ').join('+')}`;
                yield this.browser.url(url);
                let checkCount = 0;
                yield this.browser.waitUntil(() => __awaiter(this, void 0, void 0, function* () {
                    if (checkCount > 0) {
                        utils.log(`finding element with class attribute results. retry=${checkCount}`);
                    }
                    checkCount++;
                    const elem = yield this.browser.$('.results');
                    return !!elem;
                }), 1000 * 60 * 3, undefined, 1000 * 10);
                const elem = yield this.browser.$('.results');
                const text = yield elem.getText();
                const re = text.match(/について(.*)件の検索結果/);
                searchCount = re && Number(re[1]) || 0;
            }
            catch (e) {
                console.error(e);
            }
            utils.log('searched');
            return {
                count: searchCount,
            };
        });
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            this.phantom.kill();
        });
    }
}
exports.Costco = Costco;
//# sourceMappingURL=sites.js.map