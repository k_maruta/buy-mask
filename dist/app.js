"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const utils = __importStar(require("./lib/utils"));
const sites_1 = require("./lib/sites");
const models_1 = require("./lib/models");
const notice = __importStar(require("./lib/notice"));
const searchKey = 'マスク';
function exec() {
    return __awaiter(this, void 0, void 0, function* () {
        utils.log(`start`);
        try {
            const info = models_1.InfoModel.read();
            const costco = yield sites_1.Costco.create();
            const result = yield costco.search(searchKey);
            costco.close();
            let msg = '';
            let color;
            if (result.count === null) {
                msg = `NG - 検索に失敗しました。 keyword='${searchKey}'`;
                color = 'danger';
            }
            else {
                msg = `OK - 検索結果は${result.count}件です。 keyword='${searchKey}'`;
                if (result.count === 0) {
                    color = 'worning';
                }
                else if (result.count === 1) {
                    color = 'good';
                }
                else {
                    color = 'danger';
                }
            }
            utils.log(msg);
            if (info.prevCount !== result.count) {
                yield notice.slack({
                    title: '新しい状態を検知しました',
                    subtitle: null,
                    message: msg,
                    color: color,
                });
                info.prevCount = result.count;
                info.save();
            }
        }
        catch (e) {
            utils.log(`error!`);
            console.log(e);
        }
        utils.log(`end`);
    });
}
exports.exec = exec;
if (process.argv[1] === __filename) {
    exec().then(() => {
        process.exit(0);
    });
}
;
//# sourceMappingURL=app.js.map