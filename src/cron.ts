import schedule from 'node-schedule';
import { exec } from './app'

const status = schedule.scheduleJob("*/5 * * * *", async () => {
  await exec();
  console.log(`next exec ${status.nextInvocation()}`)
})

console.log(`next exec ${status.nextInvocation()}`)