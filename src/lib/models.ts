import fs from 'fs';
import path from 'path';
import * as utils from './utils';


const infoPath = path.join(__dirname, '../../data/info.json');

export class InfoModel {

    prevCount: number | null = null;

    constructor() {
        //
    }

    static read() {
        const info = new InfoModel();
        try {
            const json = fs.readFileSync(infoPath, 'utf-8')
            const data = JSON.parse(json);
            info.prevCount = data.prevCount;
        } catch(e) {
            //
        }
        return info;
    }

    save() {
        try {
            fs.writeFileSync(infoPath, JSON.stringify(this, null, 4))
            utils.log('saved info.json');
        } catch(e) {
            console.error(e)
        }
    }
}