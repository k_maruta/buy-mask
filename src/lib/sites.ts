const phantomjs = require('phantomjs-prebuilt');
import * as webdriverio from 'webdriverio';
import * as utils from './utils';

export class Costco {

    static async create() {
        const instance = new Costco();
        await instance.setup();
        return instance;
    }

    readonly baseUrl = 'https://www.costco.co.jp';
    readonly userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.1.54 (KHTML, like Gecko) Version/9.0 Safari/601.1.54";

    phantom: any = null;
    browser: webdriverio.Browser | null = null;

    constructor() {
        //
    }

    async setup() {
        const wdOpts: any = {
            hostname: 'localhost',
            port: 4444,
            capabilities: {
                browserName: 'phantomjs',
                "phantomjs.page.settings.userAgent": this.userAgent,
             },
             logLevel: 'silent',
        }
        return new Promise((resolve, reject) => {
            try {
                phantomjs.run('--webdriver=4444')
                .then(async (phantom: any) => {
                    const browser = await webdriverio.remote(wdOpts);
                    this.browser = browser;
                    this.phantom = phantom;
                    utils.log('initialized browser')
                    resolve();
                })
                .catch((e: any) => {
                    reject(e);
                })
            } catch(e) {
                reject(e);
            }
        })
    }

    async search(key: string) {
        utils.log('searching...')
        let searchCount: number | null = null;
        try {
            const url = `${this.baseUrl}/search/?text=${key.split(' ').join('+')}`;
            await this.browser!.url(url);

            let checkCount = 0;
            await this.browser!.waitUntil(async () => {
                if (checkCount > 0) {
                    utils.log(`finding element with class attribute results. retry=${checkCount}`)
                }
                checkCount++;
                const elem = await this.browser!.$('.results');
                return !!elem;
            }, 1000 * 60 * 3, undefined, 1000 * 10);
            const elem = await this.browser!.$('.results');
            const text = await elem.getText();
            const re = text.match(/について(.*)件の検索結果/);
            searchCount = re && Number(re[1]) || 0
        } catch(e) {
            console.error(e);
        }
        utils.log('searched')

        return {
            count: searchCount,
        }
    }

    async close() {
        this.phantom.kill();
    }

}
