import https from 'https';
import * as utils from './utils';
import { SlackParams } from  '../types';

export const colors = {
  good: '#2FA44F',
  worning: '#DE9E31',
  danger: '#D50200',
}

export async function slack(params: SlackParams) {
    utils.log('slack notice');

    const host = 'hooks.slack.com';
    const path = '/services/TCF3FR9T9/B011BBCAS8M/dBed3Sa4zTjGhHR0JcsEdtXP'

    let data = JSON.stringify({
      text: `${utils.nowStr()} - ${params.title}`,
      attachments: [
        {
          title: params.subtitle,
          text: params.message,
          color: colors[params.color]
        }
      ]
    });
  
    let options = {
      hostname: host,
      port: 443,
      path: path,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(data)
      }
    };
  
    return new Promise((resolve) => {
      let req = https.request(options, (res) => {
        console.log('status code : ' + res.statusCode);
        res.setEncoding('utf8');

        let body = '';
        res.on('data', (chunk) => {
          body += chunk;
        });

        res.on('end', ()=>{
          console.log(body);
          resolve();
        });
      });

      req.on('error', (e) => {
        console.error(e)
        resolve();
      ;});
    
      req.write(data);
      req.end();
  
    });
}


