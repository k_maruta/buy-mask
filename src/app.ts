import * as utils from './lib/utils';
import { Costco } from './lib/sites';
import { InfoModel } from './lib/models';
import * as notice from './lib/notice';
import { SlackColor } from './types';


const searchKey = 'マスク';

export async function exec() {
    utils.log(`start`);
    try {
        const info = InfoModel.read();

        const costco = await Costco.create();
        const result = await costco.search(searchKey);
        costco.close();

        let msg = '';
        let color: SlackColor;
        if (result.count === null) {
            msg = `NG - 検索に失敗しました。 keyword='${searchKey}'`;
            color = 'danger';
        } else {
            msg = `OK - 検索結果は${result.count}件です。 keyword='${searchKey}'`;
            if (result.count === 0) {
                color = 'worning';
            } else if (result.count === 1) {
                color = 'good';
            } else {
                color = 'danger';
            }
        }

        utils.log(msg);
        if (info.prevCount !== result.count) {
            await notice.slack({
                title: '新しい状態を検知しました',
                subtitle: null,
                message: msg,
                color: color,
            });

            info.prevCount = result.count;
            info.save()
        }


    } catch(e) {
        utils.log(`error!`);
        console.log(e);
    }
    utils.log(`end`);
}

if (process.argv[1] === __filename) {
    exec().then(() => {
        process.exit(0);
    });
};
