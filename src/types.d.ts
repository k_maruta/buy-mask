export interface SlackParams {
    title: string;
    subtitle: string | null;
    message: string;
    color: 'good' | 'worning' | 'danger';
}

export type SlackColor = 'good' | 'worning' | 'danger';